﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using System.Net;
using System.Runtime.Serialization.Json;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=391641

namespace WP_NotesBrowser
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        private const string ServerUri = "http://95.213.199.37:9000";
        private const string GetNotesUri = ServerUri + "/notes/json/all";

        private Windows.UI.Core.CoreDispatcher mCoreDispatcher;



        public MainPage()
        {
            this.InitializeComponent();

            this.NavigationCacheMode = NavigationCacheMode.Required;
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            var coreWindow = Windows.ApplicationModel.Core.CoreApplication.MainView;
            mCoreDispatcher = coreWindow.CoreWindow.Dispatcher;

            this.text_loading.Text = "Loading...";
            this.progress_ring.Visibility = Visibility.Visible;
            RequestNotesFromServer();
        }

        private async void OnRecieveNotesFromServer(IAsyncResult result)
        {
            HttpWebRequest request = result.AsyncState as HttpWebRequest;
            if (request != null)
            {
                try
                {
                    var notes = ParseJsonResponce(request.EndGetResponse(result));

                    await mCoreDispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
                    {
                        this.progress_ring.Visibility = Visibility.Collapsed;
                        this.text_loading.Visibility = Visibility.Collapsed;

                        foreach (var note in notes)
                        {
                            TextBlock textBlock = new TextBlock() { Text = note.name, FontSize = 18 };
                            this.notes_stack.Children.Add(textBlock);
                        }

                    });

                }
                catch (WebException)
                { }
            }

        }

        private List<Note> ParseJsonResponce(WebResponse responce)
        {
            var deserializer = new DataContractJsonSerializer(typeof(List<Note>));
            return (List<Note>)deserializer.ReadObject(responce.GetResponseStream());
        }

        private void RequestNotesFromServer()
        {
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(GetNotesUri);
            request.BeginGetResponse(OnRecieveNotesFromServer, request);
        }

        //---------------------------------------------
        public class Note
        {
            public string name { get; set; }
            public string time { get; set; }
            public string body { get; set; }
        }
    }

}
